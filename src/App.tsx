import { useEffect, useState } from "react";
import CurrencyList from "./Components/Lists/CurrencyList";
import { socketHandler } from "./socket";
import { currencyType } from "./core/CurrencyType";
import { Container } from "./Components/Styles/Application.style";

function App() {
  const [currencyListState, setCurrencyListState] = useState<currencyType[]>(
    []
  );

  useEffect(() => {
    const handleData = (data: currencyType[]) => {
      setCurrencyListState(data);
    };

    // Call socketHandler with the callback function
    socketHandler("wss://fstream.binance.com/stream", handleData);
  }, []);

  return (
    <Container>
      <CurrencyList dataList={currencyListState} />
    </Container>
  );
}

export default App;
