import { currencyType } from "./core/CurrencyType";

export const socketHandler = (url: string, onDataReceived: (data: currencyType[]) => void) => {

  const socket = new WebSocket(url);

  socket.onopen = () => {
    console.log("WebSocket connection established.");
    const subscribeRequest = {
      method: "SUBSCRIBE",
      params: ["!ticker@arr"],
    };
    socket.send(JSON.stringify(subscribeRequest));
  };

  socket.onmessage = (event) => {
    const eventData = JSON.parse(event.data);
    onDataReceived(eventData.data);
  };

  socket.onclose = () => {
    console.log("WebSocket connection closed.");
  };

  socket.onerror = (error) => {
    console.error("WebSocket error:", error);
  };

  // Clean up the WebSocket connection 
  return () => {
    socket.close();
  };
};
