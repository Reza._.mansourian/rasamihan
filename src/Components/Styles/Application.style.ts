import styled from "@emotion/styled";
import { CurrencyPercentageSpanProps } from "../../core/CurrencyPercentageType";

export const Container = styled.div`
  width: 100svw;
  min-height: 100dvh;
  display: flex;
  justify-content: center;
  padding: 2rem 0;
  background: #1e2026;
`;

export const SkeletonDiv = styled.div`
  width: 100%;
  height: 5rem;
  margin-bottom: 0.75rem;
  background: lightgray;
`;

export const CurrencyListUl = styled.ul`
  min-width: 20rem;
  width: 50%;
  background-color: rgb(17 24 39);
  padding: 0.25rem;
`;
export const CurrencyLi = styled.li`
  width: 100%;
  display: flex;
  justify-content: space-between;
  padding: 0.75rem;
  color: #fff;
  &:hover {
    background-color: rgb(51 65 85);
  }
`;
export const CurrencyName = styled.h4`
  font-size: 1.125rem /* 18px */;
  line-height: 1.75rem /* 28px */;
`;
export const CurrencyPerpetual = styled.p`
    font-size: 0.875rem/* 14px */;
    line-height: 1.25rem/* 20px */;
    color: rgb(107 114 128);
`;

export const CurrencyPriceDiv = styled.div`
    display: flex;
    flex-direction: column;
    align-items: end;
`;

export const CurrencyPercentageSpan = styled.span<CurrencyPercentageSpanProps>`
    font-size: 0.875rem;
    line-height: 1.25rem;
    color: ${(props) => props.percentage};
`;

