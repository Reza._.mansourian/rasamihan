
import { currencyType } from "../../core/CurrencyType";
import CurrencyItem from "../Items/CurrencyItem";
import ItemSkeleton from "../Skeleton/ItemSkeleton";
import { CurrencyListUl } from "../Styles/Application.style";

const CurrencyList = ({ dataList }: {dataList:currencyType[]}) => {
  return (
    <CurrencyListUl >
      {dataList?.length === 0
        ? Array.from({ length: 10 }, (item, index) => (
            <ItemSkeleton key={index} />
          ))
        : dataList?.map((item: currencyType) => {
            return <CurrencyItem currencyDetail={item} key={item.s} />;
          })}
    </CurrencyListUl>
  );
};

export default CurrencyList;
