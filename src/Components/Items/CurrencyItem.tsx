import { currencyType } from "../../core/CurrencyType";
import {
  CurrencyLi,
  CurrencyName,
  CurrencyPercentageSpan,
  CurrencyPerpetual,
  CurrencyPriceDiv,
} from "../Styles/Application.style";

const CurrencyItem = ({ currencyDetail }: { currencyDetail: currencyType }) => {
  return (
    <CurrencyLi>
      <div>
        <CurrencyName>{currencyDetail.s}</CurrencyName>
        <CurrencyPerpetual>Perpetual</CurrencyPerpetual>
      </div>
      <CurrencyPriceDiv>
        <p>{currencyDetail.c}</p>
        <CurrencyPercentageSpan
          percentage={
            currencyDetail.P.includes("-") ? "red" : "rgb(14, 203, 129)"
          }
        >
          {currencyDetail.P.includes("-") ? null : "+"}
          {parseFloat(currencyDetail.P).toFixed(2)}%
        </CurrencyPercentageSpan>
      </CurrencyPriceDiv>
    </CurrencyLi>
  );
};

export default CurrencyItem;
