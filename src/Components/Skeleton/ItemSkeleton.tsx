import { SkeletonDiv } from "../Styles/Application.style";

const ItemSkeleton = () => {
  return <SkeletonDiv className={`animate-pulse`}></SkeletonDiv>;
};

export default ItemSkeleton;
